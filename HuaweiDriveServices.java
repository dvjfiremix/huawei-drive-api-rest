package com.firemix.calculobtus.Services;

import android.content.Context;
import android.os.Environment;

import androidx.appcompat.app.AppCompatActivity;

import com.firemix.calculobtus.R;
import com.google.gson.Gson;
import com.huawei.cloud.base.auth.DriveCredential;
import com.huawei.cloud.services.drive.DriveScopes;
import com.huawei.hmf.tasks.Task;
import com.huawei.hms.support.api.entity.auth.Scope;
import com.huawei.hms.support.hwid.HuaweiIdAuthAPIManager;
import com.huawei.hms.support.hwid.HuaweiIdAuthManager;
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParams;
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParamsHelper;
import com.huawei.hms.support.hwid.result.AuthHuaweiId;
import com.huawei.hms.support.hwid.service.HuaweiIdAuthService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.firemix.calculobtus.Services.HDriveHelper.HWCredential;
import static com.firemix.calculobtus.Services.HDriveHelper.MIMETYPE_TXT;

public class HuaweiDriveServices extends AppCompatActivity {

    private static final Map<String, String> MIME_TYPE_MAP = new HashMap<String, String>();
    private static HuaweiDriveServices Servicios;
    private static String FileIdBackup = "";
    private static String FileIdBackupFolder = "";
    private static String FileIdImagesFolder = "";

    static {
        MIME_TYPE_MAP.put(".doc", "application/msword");
        MIME_TYPE_MAP.put(".jpg", "image/jpeg");
        MIME_TYPE_MAP.put(".mp3", "audio/x-mpeg");
        MIME_TYPE_MAP.put(".mp4", "video/mp4");
        MIME_TYPE_MAP.put(".pdf", "application/pdf");
        MIME_TYPE_MAP.put(".png", "image/png");
        MIME_TYPE_MAP.put(".txt", "text/plain");
        MIME_TYPE_MAP.put(".zip", "application/zip");
    }

    private final HWDrivesActions actions;
    private final Context ctx;
    private final HuaweiIdAuthService mAuthManager;
    private final HuaweiIdAuthParams mAuthParam;
    public DriveCredential mCredential;
    private HDriveHelper hDriveHelper;
    private String accessToken;
    private final DriveCredential.AccessMethod refreshAT = new DriveCredential.AccessMethod() {
        @Override
        public String refreshToken() {
            return accessToken;
        }
    };
    private String unionId;

    private HuaweiDriveServices(Context context, HWDrivesActions hdrivers) {
        this.actions = hdrivers;
        this.ctx = context;
        List<Scope> scopeList = new ArrayList<>();
        scopeList.add(new Scope(DriveScopes.SCOPE_DRIVE_FILE));
        scopeList.add(new Scope(DriveScopes.SCOPE_DRIVE));
        scopeList.add(new Scope(DriveScopes.SCOPE_DRIVE_APPDATA));
        scopeList.add(HuaweiIdAuthAPIManager.HUAWEIID_BASE_SCOPE);
        mAuthParam = new HuaweiIdAuthParamsHelper(HuaweiIdAuthParams.DEFAULT_AUTH_REQUEST_PARAM)
                .setIdToken()
                .setAccessToken()
                .setScopeList(scopeList)
                .createParams();
        mAuthManager = HuaweiIdAuthManager.getService(context, mAuthParam);
        Task<AuthHuaweiId> task = mAuthManager.silentSignIn();
        task.addOnSuccessListener(authHuaweiId -> {
            accessToken = authHuaweiId.getAccessToken();
            unionId = authHuaweiId.getUnionId();
            DriveCredential.Builder builder = new DriveCredential.Builder(unionId, refreshAT);
            mCredential = builder.build().setAccessToken(accessToken);
            if (mCredential != null) {
                if (authHuaweiId != null) {
                    hDriveHelper = new HDriveHelper(HWCredential(context, mCredential));
                    actions.Iniciar();
                    actions.GDriveSession(true, authHuaweiId.getAvatarUriString(), authHuaweiId.getDisplayName());
                }
            }
        });
        task.addOnFailureListener(e -> {
            actions.GDriveSession(false, "", "");
        });
    }

    public static HuaweiDriveServices getInstance(Context context, HWDrivesActions hdrivers) {
        if (context != null) {
            Servicios = new HuaweiDriveServices(context, hdrivers);
        }
        return Servicios;
    }

    /* Acciones */

    //Buscar el folder solicitado
    public void SearchFolder(String FolderName) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.searchFolder(FolderName)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            JSONObject json_data = jArray.getJSONObject(0);
                            if (!json_data.getString("id").equals("")) {
                                FileIdBackupFolder = json_data.getString("id");
                                SearchFile();
                            }
                        } else {
                            CreateFolder();
                        }
                    } catch (JSONException e) {
                        actions.ErrorGDrive(e.getMessage());
                    }
                })
                .addOnFailureListener(e -> {
                    actions.ErrorGDrive(e.getMessage());
                    actions.SuccesBackup(false);
                });
    }

    //Buscar el folder solicitado
    public void SearchFolderImages(String ImagesFolder) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.searchFolder(ImagesFolder)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            JSONObject json_data = jArray.getJSONObject(0);
                            if (!json_data.getString("id").equals("")) {
                                FileIdBackupFolder = json_data.getString("id");
                                ListFolder(FileIdBackupFolder);
                            }
                        } else {
                            SearchFolderId();
                        }
                    } catch (JSONException e) {
                        actions.ErrorGDrive(e.getMessage());
                    }
                })
                .addOnFailureListener(e -> {
                    actions.ErrorGDrive(e.getMessage());
                    actions.SuccesBackup(false);
                });
    }

    //Buscar el folder solicitado
    public void SearchFolderImagesDownload() {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.searchFolder(BackImagesFolder)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            JSONObject json_data = jArray.getJSONObject(0);
                            if (!json_data.getString("id").equals("")) {
                                FileIdImagesFolder = json_data.getString("id");
                                ListFolderDownload();
                            }
                        }
                    } catch (JSONException e) {
                        actions.ErrorGDrive(e.getMessage());
                    }
                })
                .addOnFailureListener(e -> {
                    actions.ErrorGDrive(e.getMessage());
                    actions.SuccesBackup(false);
                });
    }

    //Buscar el folder solicitado
    public void SearchFolderId(String BackFolder) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.searchFolder(BackFolder)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            JSONObject json_data = jArray.getJSONObject(0);
                            if (!json_data.getString("id").equals("")) {
                                FileIdBackupFolder = json_data.getString("id");
                                CreateSubFolder(SubFolderName,FileIdBackupFolder);
                            }
                        } else {
                            actions.InicializeProcess(FileIdBackupFolder);
                        }
                    } catch (JSONException e) {
                        actions.ErrorGDrive(e.getMessage());
                    }
                })
                .addOnFailureListener(e -> {
                    actions.ErrorGDrive(e.getMessage());
                    actions.SuccesBackup(false);
                });
    }

    //Create Folder Backup
    private void CreateFolder(Sring FolderName) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.createFolder(FolderName, null)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONObject json_data = new JSONObject(gson.toJson(HDriveHolders));
                            if (!json_data.getString("id").equals("")) {
                                FileIdBackupFolder = json_data.getString("id");
                                SearchFile();
                            }
                        } else {
                            actions.ErrorGDrive(getString(R.string.folder_not_created));
                            actions.SuccesBackup(false);
                        }
                    } catch (JSONException e) {
                        actions.ErrorGDrive("Error: " + e.getMessage());
                        actions.SuccesBackup(false);
                    }
                })
                .addOnFailureListener(e -> {
                    actions.ErrorGDrive(e.getMessage());
                    actions.SuccesBackup(false);
                });
    }

    //Create Folder Backup
    private void CreateSubFolder(String ParentFolderId, String SubFolderName) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.createFolder(SubFolderName, ParentFolderId)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONObject json_data = new JSONObject(gson.toJson(HDriveHolders));
                            if (!json_data.getString("id").equals("")) {
                                FileIdBackupFolder = json_data.getString("id");
                                actions.InicializeProcess(FileIdBackupFolder);
                            }
                        } else {
                            actions.ErrorGDrive(getString(R.string.folder_not_created));
                            actions.SuccessSearchProccess(false);
                        }
                    } catch (JSONException e) {
                        actions.ErrorGDrive("Error: " + e.getMessage());
                        actions.SuccessSearchProccess(false);
                    }
                })
                .addOnFailureListener(e -> {
                    actions.ErrorGDrive(e.getMessage());
                    actions.SuccessSearchProccess(false);
                });
    }

    //Buscar el archivo
    private void SearchFile(String NombreArchivo, String Myme_Type) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.searchFile(NombreArchivo, Myme_Type)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            JSONObject json_data = jArray.getJSONObject(0);
                            if (!json_data.getString("id").equals("")) {
                                FileIdBackup = json_data.getString("id");
                                DeleBackupFile();
                            }
                        } else {
                            UploadFile();
                        }
                    } catch (JSONException e) {
                        actions.ErrorGDrive("Error: " + e.getMessage());
                    }
                })
                .addOnFailureListener(e -> {
                    actions.ErrorGDrive(e.getMessage());
                });

    }

    //Buscar el archivo
    public void SearchImageFile(ArrayList<File> imagenes, String FolderId) {
        if (hDriveHelper == null) {
            return;
        }
        for (int cuenta = 0; cuenta < imagenes.size(); cuenta++) {
            int finalCuenta = cuenta;
            hDriveHelper.searchFile(imagenes.get(cuenta).getName(), mimeType(imagenes.get(cuenta)))
                    .addOnSuccessListener(HDriveHolders -> {
                        Gson gson = new Gson();
                        try {
                            if (!gson.toJson(HDriveHolders).equals("[]")) {
                                JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                                JSONObject json_data = jArray.getJSONObject(0);
                                if (!json_data.getString("id").equals("")) {
                                    FileIdBackup = json_data.getString("id");
                                    DeleBackupFileImage(imagenes.get(finalCuenta), finalCuenta, imagenes.size(), FileIdBackup, FolderId);
                                }
                            } else {
                                UploadFileImages(imagenes.get(finalCuenta), finalCuenta, imagenes.size(), FolderId);
                            }
                        } catch (JSONException e) {
                            actions.ErrorGDrive("Error: " + e.getMessage());
                        }
                    })
                    .addOnFailureListener(e -> {
                        actions.ErrorGDrive(e.getMessage());
                    });
        }
    }

    //Buscar el folder de imágenes para descargar
    public void SearchFileToDownload(String Archivo, String Myme_Type) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.searchFile(Archivo, Myme_Type)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            JSONObject json_data = jArray.getJSONObject(0);
                            if (!json_data.getString("id").equals("")) {
                                FileIdBackup = json_data.getString("id");
                                DownloadFile();
                            }
                        } else {
                            actions.DownloadFile(false);
                        }
                    } catch (JSONException e) {
                        actions.ErrorGDrive(e.getMessage());
                    }
                })
                .addOnFailureListener(e -> {
                    actions.ErrorGDrive(e.getMessage());
                });

    }

    //UploadFile to Folder
    private void UploadFile(String FileIdBackupFolder, java.io.File Archivo) {
        if (hDriveHelper == null) {
            return;
        }
        try {
           
            hDriveHelper.uploadFile(Archivo, FileIdBackupFolder)
                    .addOnSuccessListener(HdriveHolders -> {
                        actions.SuccesBackup(true);
                    })
                    .addOnFailureListener(e -> {
                        actions.SuccesBackup(false);
                        actions.ErrorGDrive(e.getMessage());
                    });
        } catch (Exception e) {
            actions.ErrorGDrive(e.getMessage());
            actions.SuccesBackup(false);
        }
    }

    //UploadFile to Folder
    private void UploadFileImages(File imagen, int num, int total, String Folders) {
        if (hDriveHelper == null) {
            return;
        }
        try {
            hDriveHelper.uploadFile(imagen, Folders)
                    .addOnSuccessListener(HdriveHolders -> {
                        actions.getProgressUpload(num + 1, total);
                        if (num == total - 1) {
                            actions.UploadFilesImages(true);
                        }
                    })
                    .addOnFailureListener(e -> {
                        actions.UploadFilesImages(false);
                        actions.ErrorGDrive(e.getMessage());
                    });
        } catch (Exception e) {
            actions.ErrorGDrive(e.getMessage());
            actions.UploadFilesImages(false);
        }
    }

    //Eliminar el archivo si existe
    private void DeleBackupFile(String FileIdBackup) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.deleteFolderFile(FileIdBackup).addOnSuccessListener(aVoid -> {
            UploadFile();
        }).addOnFailureListener(e -> {
            actions.ErrorGDrive(e.getMessage());
        });
    }

    //Eliminar el archivo si existe
    private void DeleFiles(String imagenId) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.deleteFolderFile(imagenId).addOnSuccessListener(aVoid -> {
        }).addOnFailureListener(e -> {
            actions.ErrorGDrive(e.getMessage());
        });
    }

    //Eliminar el archivo si existe
    private void DeleBackupFileImage(File imagen, int num, int total, String FileImagen, String FolderID) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.deleteFolderFile(FileImagen).addOnSuccessListener(aVoid -> {
            UploadFileImages(imagen, num, total, FolderID);
        }).addOnFailureListener(e -> {
            actions.ErrorGDrive(e.getMessage());
        });
    }

    //DowloadFile
    private void DownloadFile(String FileId, java.io.File BackupFile) {
        if (hDriveHelper == null) {
            return;
        }
    
        if (BackupFile.exists()) {
            BackupFile.delete();
        }

        hDriveHelper.downloadFile(BackupFile, FileId)
                .addOnSuccessListener(aVoid -> {
                    actions.DownloadFile(true);
                })
                .addOnFailureListener(e -> {
                    actions.ErrorGDrive(e.getMessage());
                    actions.DownloadFile(false);
                });
    }

    //Dowload Files Images to Folder
    private void DownloadFileImages(java.io.File archivo, String ID, int Total, int numero) {
        if (hDriveHelper == null) {
            return;
        }
        if (archivo.exists()) {
            archivo.delete();
        }
        hDriveHelper.downloadFile(archivo, ID)
                .addOnSuccessListener(aVoid -> {
                    actions.getProgressUpload(numero, Total);
                    if (numero == Total) {
                        actions.DownloadImages(true);
                    }
                })
                .addOnFailureListener(e -> {
                    actions.ErrorGDrive(e.getMessage());
                    actions.DownloadFile(false);
                    actions.DownloadImages(false);
                });
    }

    //ListFilesFolder
    private void ListFolder(String Folder) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.queryFiles(Folder)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject json_data = jArray.getJSONObject(i);
                                if (!json_data.getString("id").equals("")) {
                                    String idImagen = json_data.getString("id");
                                    DeleFiles(idImagen);
                                }
                            }
                            actions.InicializeProcess(Folder);
                        }
                    } catch (JSONException e) {
                        actions.ErrorGDrive("Error: " + e.getMessage());
                    }
                })
                .addOnFailureListener(e -> {
                });
    }

    //ListFilesFolder de fotos para cargar
    private void ListFolderDownload(Sting FileIdImagesFolder) {
        if (hDriveHelper == null) {
            return;
        }
        hDriveHelper.queryFiles(FileIdImagesFolder)
                .addOnSuccessListener(HDriveHolders -> {
                    Gson gson = new Gson();
                    try {
                        if (!gson.toJson(HDriveHolders).equals("[]")) {
                            JSONArray jArray = new JSONArray(gson.toJson(HDriveHolders));
                            int Cuenta = jArray.length();
                            int numero = 1;
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject json_data = jArray.getJSONObject(i);
                                if (!json_data.getString("id").equals("")) {
                                    String idImagen = json_data.getString("id");
                                    String Foto = json_data.getString("fileName");
                                    java.io.File BackupFile;
                                    BackupFile = new java.io.File(Environment.getExternalStorageDirectory(), "Ruta/"+Foto);
                                    DownloadFileImages(BackupFile, idImagen, Cuenta, numero);
                                    numero++;
                                }
                            }
                        }
                    } catch (JSONException e) {
                        actions.ErrorGDrive("Error: " + e.getMessage());
                        actions.DownloadImages(false);
                    }
                })
                .addOnFailureListener(e -> {

                });
    }

    private String mimeType(java.io.File file) {
        if (file != null && file.exists() && file.getName().contains(".")) {
            String fileName = file.getName();
            String suffix = fileName.substring(fileName.lastIndexOf("."));
            if (MIME_TYPE_MAP.containsKey(suffix)) {
                return MIME_TYPE_MAP.get(suffix);
            }
        }
        return "*/*";
    }


    public interface HWDrivesActions {
        void GDriveSession(boolean login, String image, String username);

        void ErrorGDrive(String Message);

        void SuccesBackup(boolean state);

        void DownloadFile(boolean state);

        void UploadFilesImages(boolean state);

        void InicializeProcess(String FolderId);

        void SuccessSearchProccess(boolean estado);

        void getProgressUpload(int progreso, int total);

        void DownloadImages(boolean estado);

        void Iniciar();
    }
}
