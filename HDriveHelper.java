package com.firemix.calculobtus.Services;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.huawei.cloud.base.auth.DriveCredential;
import com.huawei.cloud.base.http.AbstractInputStreamContent;
import com.huawei.cloud.base.http.FileContent;
import com.huawei.cloud.services.drive.Drive;
import com.huawei.cloud.services.drive.model.File;
import com.huawei.cloud.services.drive.model.FileList;
import com.huawei.hmf.tasks.Task;
import com.huawei.hmf.tasks.Tasks;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class HDriveHelper {
    private static final Map<String, String> MIME_TYPE_MAP = new HashMap<String, String>();
    public static String MIMETYPE_FOLDER = "application/vnd.huawei-apps.folder";
    public static String MIMETYPE_TXT = "text/plain";
    public static String MIMETYPE_PNG = "image/png";
    public static String MIMETYPE_JPG = "image/jpeg";
    public static String MIMETYPE_JPEG = "application/zip";
    public static String MIMETYPE_PDF = "application/pdf";

    static {
        MIME_TYPE_MAP.put(".doc", "application/msword");
        MIME_TYPE_MAP.put(".jpg", "image/jpeg");
        MIME_TYPE_MAP.put(".mp3", "audio/x-mpeg");
        MIME_TYPE_MAP.put(".mp4", "video/mp4");
        MIME_TYPE_MAP.put(".pdf", "application/pdf");
        MIME_TYPE_MAP.put(".png", "image/png");
        MIME_TYPE_MAP.put(".txt", "text/plain");
        MIME_TYPE_MAP.put(".zip", "application/zip");
    }

    private final Executor mExecutor = Executors.newSingleThreadExecutor();
    private final Drive drive;

    public HDriveHelper(Drive driveService) {
        drive = driveService;
    }

    public static Drive HWCredential(Context ctx, DriveCredential mCredential) {
        Drive service = new Drive.Builder(mCredential, ctx).build();
        return service;
    }

    public Task<String> createFile(final String fileName) {
        return Tasks.callInBackground(mExecutor, new Callable<String>() {
            @Override
            public String call() throws Exception {
                File metadata = new File()
                        .setParentFolder(Collections.singletonList("root"))
                        .setMimeType(MIMETYPE_TXT)
                        .setFileName(fileName);

                File HDrive = drive.files().create(metadata).execute();
                if (HDrive == null) {
                    throw new IOException("Null result when requesting file creation.");
                }

                return HDrive.getId();
            }
        });
    }

    public Task<String> createFile(final String fileName, @Nullable final String folderId) {
        return Tasks.callInBackground(mExecutor, new Callable<String>() {
            @Override
            public String call() throws Exception {
                List<String> root;
                if (folderId == null) {
                    root = Collections.singletonList("root");
                } else {

                    root = Collections.singletonList(folderId);
                }

                File metadata = new File()
                        .setParentFolder(root)
                        .setMimeType(MIMETYPE_TXT)
                        .setFileName(fileName);

                File HDrive = drive.files().create(metadata).execute();
                if (HDrive == null) {
                    throw new IOException("Null result when requesting file creation.");
                }

                return HDrive.getId();
            }
        });
    }

    public Task<File> createFolder(final String folderName, @Nullable final String folderId) {
        return Tasks.callInBackground(mExecutor, new Callable<File>() {
            @Override
            public File call() throws Exception {
                File HDrive = new File();

                List<String> root;
                if (folderId == null) {
                    root = Collections.singletonList("root");
                } else {

                    root = Collections.singletonList(folderId);
                }
                File metadata = new File()
                        .setParentFolder(root)
                        .setMimeType(MIMETYPE_FOLDER)
                        .setFileName(folderName);

                File HWFile = drive.files().create(metadata).execute();
                if (HWFile == null) {
                    throw new IOException("Null result when requesting file creation.");
                }
                HDrive.setId(HWFile.getId());
                return HDrive;
            }
        });
    }

    public Task<Pair<String, String>> readFile(final String fileId) {
        return Tasks.callInBackground(mExecutor, new Callable<Pair<String, String>>() {
            @Override
            public Pair<String, String> call() throws Exception {
                // Retrieve the metadata as a File object.
                File metadata = drive.files().get(fileId).execute();
                String name = metadata.getFileName();

                // Stream the file contents to a String.
                try (InputStream is = drive.files().get(fileId).executeContentAsInputStream();
                     BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    String contents = stringBuilder.toString();

                    return Pair.create(name, contents);
                }
            }
        });
    }

    public Task<Void> deleteFolderFile(final String fileId) {
        return Tasks.callInBackground(mExecutor, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                // Retrieve the metadata as a File object.
                if (fileId != null) {
                    drive.files().delete(fileId).execute();
                }

                return null;
            }
        });
    }

    public Task<File> uploadFile(final File HFile, final AbstractInputStreamContent content) {
        return Tasks.callInBackground(mExecutor, new Callable<File>() {
            @Override
            public File call() throws Exception {
                // Retrieve the metadata as a File object.
                File fileMeta = drive.files().create(HFile, content).execute();
                File HWFileFolder = new File();
                HWFileFolder.setId(fileMeta.getId());
                HWFileFolder.setFileName(fileMeta.getFileName());
                return HWFileFolder;
            }
        });
    }

    public Task<File> uploadFile(final java.io.File localFile, @Nullable final String folderId) {
        return Tasks.callInBackground(mExecutor, () -> {
            List<String> root;
            if (folderId == null) {
                root = Collections.singletonList("root");
            } else {

                root = Collections.singletonList(folderId);
            }
            String mimes = mimeType(localFile);

            File metadata = new File()
                    .setParentFolder(root)
                    .setMimeType(mimes)
                    .setFileName(localFile.getName());

            return drive.files().create(metadata, new FileContent(mimes, localFile)).setFields("*").execute();
        });
    }

    public Task<List<File>> searchFolder(final String folderName) {
        return Tasks.callInBackground(mExecutor, () -> {
            List<File> HuaweiFileList = new ArrayList<>();
            FileList result = drive.files().list()
                    .setQueryParam("mimeType = '" + MIMETYPE_FOLDER + "' and fileName = '" + folderName + "' ")
                    .execute();

            for (int i = 0; i < result.getFiles().size(); i++) {

                File HDriveFile = new File();
                HDriveFile.setId(result.getFiles().get(i).getId());
                HDriveFile.setFileName(result.getFiles().get(i).getFileName());

                HuaweiFileList.add(HDriveFile);
            }
            return HuaweiFileList;
        });
    }

    public Task<List<File>> searchFile(final String fileName, final String mimeType) {
        return Tasks.callInBackground(mExecutor, new Callable<List<File>>() {
            @Override
            public List<File> call() throws Exception {
                List<File> HDriveList = new ArrayList<>();
                // Retrive the metadata as a File object.
                FileList result = drive.files().list()
                        .setQueryParam("fileName = '" + fileName + "' and mimeType = '" + mimeType + "'")
                        .setFields("files(id,fileName,size,createdTime,editedTime)")
                        .execute();
                Log.d("Downloads: ", new Gson().toJson(result));

                for (int i = 0; i < result.getFiles().size(); i++) {
                    File HDriveFile = new File();
                    HDriveFile.setId(result.getFiles().get(i).getId());
                    HDriveFile.setFileName(result.getFiles().get(i).getFileName());
                    HDriveFile.setEditedTime(result.getFiles().get(i).getEditedTime());
                    HDriveFile.setSize(result.getFiles().get(i).getSize());
                    HDriveFile.setMimeType(result.getFiles().get(i).getMimeType());
                    HDriveList.add(HDriveFile);
                }

                return HDriveList;
            }
        });
    }

    private String mimeType(java.io.File file) {
        if (file != null && file.exists() && file.getName().contains(".")) {
            String fileName = file.getName();
            String suffix = fileName.substring(fileName.lastIndexOf("."));
            if (MIME_TYPE_MAP.containsKey(suffix)) {
                return MIME_TYPE_MAP.get(suffix);
            }
        }
        return "*/*";
    }


    public Task<Void> downloadFile(final java.io.File fileSaveLocation, final String fileId) {
        return Tasks.callInBackground(mExecutor, new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                // Retrieve the metadata as a File object.
                OutputStream outputStream = new FileOutputStream(fileSaveLocation);
                drive.files().get(fileId).executeContentAndDownloadTo(outputStream);
                return null;
            }
        });
    }

    public Task<InputStream> downloadFile(final String fileId) {
        return Tasks.callInBackground(mExecutor, new Callable<InputStream>() {
            @Override
            public InputStream call() throws Exception {
                // Retrieve the metadata as a File object.
                return drive.files().get(fileId).executeContentAsInputStream();
            }
        });
    }

    public Task<List<File>> queryFiles() {
        return Tasks.callInBackground(mExecutor, new Callable<List<File>>() {
                    @Override
                    public List<File> call() throws Exception {
                        List<File> HDriveList = new ArrayList<>();


                        FileList result = drive.files().list().setFields("files(id, fileName,size,createdTime,modifiedTime,starred,mimeType)").
                                setFields("drive").execute();

                        for (int i = 0; i < result.getFiles().size(); i++) {

                            File HDrive = new File();
                            HDrive.setId(result.getFiles().get(i).getId());
                            HDrive.setFileName(result.getFiles().get(i).getFileName());
                            if (result.getFiles().get(i).getSize() != null) {
                                HDrive.setSize(result.getFiles().get(i).getSize());
                            }

                            if (result.getFiles().get(i).getEditedTime() != null) {
                                HDrive.setEditedTime(result.getFiles().get(i).getEditedTime());
                            }

                            if (result.getFiles().get(i).getCreatedTime() != null) {
                                HDrive.setCreatedTime(result.getFiles().get(i).getCreatedTime());
                            }

                            if (result.getFiles().get(i).getMimeType() != null) {
                                HDrive.setMimeType(result.getFiles().get(i).getMimeType());
                            }
                            HDriveList.add(HDrive);

                        }


                        return HDriveList;


                    }
                }
        );
    }

    public Task<List<File>> queryFiles(@Nullable final String folderId) {
        return Tasks.callInBackground(mExecutor, new Callable<List<File>>() {
                    @Override
                    public List<File> call() throws Exception {
                        List<File> HDriveList = new ArrayList<>();
                        String parent = "root";
                        if (folderId != null) {
                            parent = folderId;
                        }

                        FileList result = drive.files().list()
                                .setQueryParam("'" + parent + "' in parentFolder")
                                .setOrderBy("fileName")
                                .setFields("category,nextCursor,files(id,fileName,size)")
                                .setContainers("")
                                .execute();

                        for (int i = 0; i < result.getFiles().size(); i++) {

                            File HDrive = new File();
                            HDrive.setId(result.getFiles().get(i).getId());
                            HDrive.setFileName(result.getFiles().get(i).getFileName());
                            if (result.getFiles().get(i).getSize() != null) {
                                HDrive.setSize(result.getFiles().get(i).getSize());
                            }

                            if (result.getFiles().get(i).getEditedTime() != null) {
                                HDrive.setEditedTime(result.getFiles().get(i).getEditedTime());
                            }

                            if (result.getFiles().get(i).getCreatedTime() != null) {
                                HDrive.setCreatedTime(result.getFiles().get(i).getCreatedTime());
                            }

                            if (result.getFiles().get(i).getCreatedTime() != null) {
                                HDrive.setCreatedTime(result.getFiles().get(i).getCreatedTime());
                            }
                            if (result.getFiles().get(i).getMimeType() != null) {
                                HDrive.setMimeType(result.getFiles().get(i).getMimeType());
                            }

                            HDriveList.add(HDrive);

                        }


                        return HDriveList;


                    }
                }
        );
    }
}
