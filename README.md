# Huawei Drive API Rest

Este es un repo donde adjunto 2 clases para poder hacer uso de Huawei Drive y poder hacer las siguientes acciones:
* Subir archivos
* Descargar Archivos
* Buscar Archivos
* Crear Carpetas
* Listar Archivos
* Eliminar archivos

Cómo se usa es muy sencillo primero deben copiar estás 2 clases en su proyecto:
- HDriveHelper.java
- HuaweiDriveServices.java

Paso 2: Necesitaran agregar las siguientes dependencias en su proyecto, Vincular el proyecto en la consola de Huawei y cargar el archivo agconnect-services.json:   
- implementation 'com.huawei.hms:drive:5.0.0.300'
- implementation 'com.huawei.hms:hwid:4.0.0.300'
- implementation 'com.huawei.agconnect:agconnect-core:1.4.2.301'




Paso 3: Para poder usar la librería solo deben de crear un Objeto de la clase donde lo vayan a requerir y deberan instanciarlo una vez el usuario esté autenticado 
con su Huawei ID e implementar las interfaces:
new HuaweiDriveServices.getInstance(this,this);

    //Obtiene la imagen de usuario del ID de Huawei y nombre de usuario
    void GDriveSession(boolean login, String image, String username);

    //Regresa un error en caso de haberlo
    void ErrorGDrive(String Message);

    //Obtiene el estado del respaldo true or false
    void SuccesBackup(boolean state);

    //Verifica si el archivo para descargar existe, si es así procede a descargarlo.
    void DownloadFile(boolean state);

    //Verifica que las imagenes hayan sido subidas
    void UploadFilesImages(boolean state);

    //Este es usado para poder buscar un folder mediante el ID de este
    void InicializeProcess(String FolderId);

    //Verifica si se encontró o no algún resultado
    void SuccessSearchProccess(boolean estado);

    //Regresa el porcentaje que lleva de los archivos que esté subiendo
    void getProgressUpload(int progreso, int total);

    //Verifica si las imágenes se han descargado
    void DownloadImages(boolean estado);

    //Este es muy importante ya que se debe usar en un service para poder iniciar una vez detectó que el usuario ha sido autenticado para poder iniciar los procesos
    void Iniciar();

Ejemplos:
**HuaweiDriveServices Hdrive new HuaweiDriveServices.getInstance(this, this);**

**Crear una carpeta en Huawei Drive:**
Hdrive.CreateFolder(NombreFolder);

**Crear un SubFolder dentro de otro:**
Hdrive.CreateSubFolder(IDFolder,NombreSubFolder);

**Busca el folder por Nombre y obtiene el ID**
Hdrive.SearchFolderId(FolderName);

**Busca un archivo por Nombre y MYME_TYPE**
Hdrive.SearchFile(FileName,MYME_TYPE);

**Busca un archivo por nombre y MYME_TYPE para poder descargarlo;**
Hdrive.SearchFileToDownload(Nombre,MYME_TYPE);

**Sube un archivo tipo java.io.File Archivo a un folder especifico por el ID:**
Hdrive.UploadFile(FolderId, Archivo);

**Elimina un archivo por ID si existe:**
Hdrive.DeleBackupFile(FileId);

**Descarga un archivo por ID, requiere ID del archivo y ruta destino tipo java.io.File:**
Hdrive.DownloadFile(FileId,Archivo);

**Lista los archivos de un folder:**
Hdrive.ListFolder(FolderName);

Video Demostrativo:
[Youtube](https://www.youtube.com/watch?v=pDWuhcoQQrA)
